function layThongTuForm() {
    var ma = document.getElementById('txtMaSV').value
    var ten = document.getElementById('txtTenSV').value
    var email = document.getElementById('txtEmail').value
    var hinhAnh = document.getElementById('txtImg').value
    return {
        ma: ma,
        ten: ten,
        email: email,
        hinhAnh: hinhAnh,
    }
}

function showThongTinLenForm(data) {
    document.getElementById('txtMaSV').value = data.ma
    document.getElementById('txtTenSV').value = data.ten
    document.getElementById('txtEmail').value = data.email
    document.getElementById('txtImg').value = data.hinhAnh
}