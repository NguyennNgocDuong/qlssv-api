var dssv = [];
const BASE_URL = "https://62f8b754e0564480352bf3cc.mockapi.io";

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

function renderTable(listDssv) {
  let contentHTML = "";
  listDssv.map((sv) => {
    let trContent = `
    <tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>
    <img src="${sv.hinhAnh}" style="width:80px" alt="" />
    </td>
    <td>0</td>
    <td>
    <button class="btn btn-danger" onclick="xoaSV(${sv.ma})">Xóa</button>
    <button class="btn btn-warning"  onclick="suaSV(${sv.ma})">Sửa</button>
    </td>
    </tr>
      `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function renderDssvService() {
  batLoading();
  document.getElementById("txtMaSV").disabled = true;

  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch((err) => {
      tatLoading();

    });
}
renderDssvService();

function themSV() {
  let dataForm = layThongTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then((res) => {
      tatLoading();
      renderDssvService();
    })
    .catch((err) => {
      tatLoading();

    });
}

function xoaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then((res) => {

      tatLoading();
      renderDssvService();

    })
    .catch((err) => {
      tatLoading();

    });
}

function suaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      tatLoading();

    });
}

function capNhatSV() {
  let dataForm = layThongTuForm();

  batLoading();
  document.getElementById("txtMaSV").value = "";

  axios({
    url: `${BASE_URL}/sv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then((res) => {
      tatLoading();
      document.getElementById("txtMaSV").value = "";
      renderDssvService();
    })
    .catch((err) => {
      tatLoading();

    });
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
